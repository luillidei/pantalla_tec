
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "tft_controller.h"

void app_main(void)
{
   _init_TFT();
   draw_text();
}
